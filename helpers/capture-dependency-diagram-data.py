#!/usr/bin/python3
import os
import sys
import tarfile
import tempfile
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler, Packages

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to capture dependency diagrams (from CMake) for later consumption by api.kde.org')
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--usingInstall', type=str, required=True)
parser.add_argument('--environment', type=str, required=True)
arguments = parser.parse_args()

# Are we interested in gathering this information?
# We need to be running on the principal Linux platform - as it has the most capacity (and we can only build apidocs once)
if arguments.platform != 'SUSEQt5.14' and arguments.platform != 'SUSEQt5.15':
	# Then there is nothing for us to do
	sys.exit(0)

# First determine where we the data will be stored, both temporarily and on the server
sourcesLocation = os.getcwd()
outputDirectory = os.path.join( sourcesLocation, 'dotdata' )

# Determine the environment we need to provide for the compilation process
buildEnvironment = EnvironmentHandler.generateFor( installPrefix=arguments.usingInstall )

# Build up the command to run
commandToRun = 'python3 "{0}/kapidox/src/depdiagram-prepare" -s "{1}" "{2}"'
commandToRun = commandToRun.format( CommonUtils.scriptsBaseDirectory(), sourcesLocation, outputDirectory )

# Run the command, which will generate a pile of *.dot files for us
process = subprocess.Popen( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, env=buildEnvironment )
process.wait()

# Do we have something to upload?
if not os.path.exists(outputDirectory):
	# Then exit gracefully, nothing for us to do!
	sys.exit(0)

# Now we can capture what we've generated

# Start by creating a tar archive...
archiveFile = tempfile.NamedTemporaryFile(delete=False)
archive = tarfile.open( fileobj=archiveFile, mode='w' )

# Add all the files which need to be in the archive into the archive
# We want to capture the tree as it is inside the install directory and don't want any trailing slashes in the archive as this isn't standards compliant
# Therefore we list everything in the install directory and add each of those to the archive, rather than adding the whole install directory
filesToInclude = os.listdir( outputDirectory )
for filename in filesToInclude:
	fullPath = os.path.join(outputDirectory, filename)
	archive.add( fullPath, arcname=filename, recursive=True )

# Close the archive, which will write it out to disk, finishing what we need to do here
# This is also necessary on Windows to allow for storePackage to move it to it's final home
archive.close()
archiveFile.close()

# Initialize the archive manager
ourArchive = Packages.Archive( arguments.environment, 'DependencyDiagrams' )

# Add the package to the archive
ourArchive.storePackage( arguments.project, archiveFile.name, '' )

# All done!
sys.exit(0)
