#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
from helperslib import BuildSpecs, BuildSystem, CommonUtils, EnvironmentHandler

# Parse the command line arguments we've been given
parser = argparse.ArgumentParser(description='Utility to determine whether an application is appstream compliant.')
parser.add_argument('--product', type=str, required=True)
parser.add_argument('--project', type=str, required=True)
parser.add_argument('--branchGroup', type=str, required=True)
parser.add_argument('--platform', type=str, required=True)
parser.add_argument('--usingInstall', type=str, required=True)
parser.add_argument('--withDiverted', type=str, required=True)
arguments = parser.parse_args()

# Load our build specification, which governs how we handle this build
buildSpecification = BuildSpecs.Loader( product=arguments.product, project=arguments.project, branchGroup=arguments.branchGroup, platform=arguments.platform )

# Determine the environment we need to provide for the compilation process
buildEnvironment = EnvironmentHandler.generateFor( installPrefix=arguments.usingInstall )

# If we aren't running on Linux, or if Appstream Check's have been disabled for this build, bail
# Appstream isn't relevant outside Linux, so even if the tooling was available (probably not) there is little point running the checks there
if sys.platform != "linux" or not buildSpecification['do-appstream-check']:
	# Bail!
	sys.exit(0)

# Before we can process it, we need to determine which prefix to check
installRoot = arguments.usingInstall
if arguments.withDiverted != None:
	installRoot = os.path.join( arguments.withDiverted, CommonUtils.makePathRelative(arguments.usingInstall) )

# Make sure we have Appstream metadata to process
appstreamDirectory = os.path.join( installRoot, 'share', 'metainfo' )
if not os.path.exists( appstreamDirectory ):
	# Bail!
	sys.exit(0)

# Determine the command we need to run
commandToRun = "appstreamcli validate --pedantic '{0}'/*"
commandToRun = commandToRun.format( appstreamDirectory )

# Now run it!
try:
	subprocess.check_call( commandToRun, stdout=sys.stdout, stderr=sys.stderr, shell=True, env=buildEnvironment )
except Exception:
	sys.exit(0)

# The project passed appstream validation successfully
sys.exit(0)
