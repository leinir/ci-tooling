#!/usr/bin/python3
import os
import sys
import argparse
import subprocess
import yaml
from helperslib import BuildSpecs, Buildable, CommonUtils, EnvironmentHandler

parser = argparse.ArgumentParser(description='Utility to check if metainfo.yaml is lying.', formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('metainfo', nargs='+', help='metainfo.yaml files', type=str)
arguments = parser.parse_args()

allPlatforms = {
    'SUSEQt5.12': 'Linux',
    'SUSEQt5.13': 'Linux',
    'SUSEQt5.14': 'Linux',
    'SUSEQt5.15': 'Linux',
    'FreeBSDQt5.15': 'FreeBSD',
    'WindowsMSVCQt5.15': 'Windows',
    None: 'MacOSX',
    'AndroidQt5.15': 'Android'
}

resolver = Buildable.DependencyResolver()
resolver.loadProjectsFromTree(os.path.join( CommonUtils.scriptsBaseDirectory(), 'repo-metadata', 'projects-invent' ))
resolver.loadProjectsIgnoreRules(os.path.join( CommonUtils.scriptsBaseDirectory(), 'local-metadata', 'project-ignore-rules.yaml' ))

for metainfo in arguments.metainfo:
    metainfoFile = open(metainfo, 'r', encoding='utf-8')
    read = yaml.safe_load(metainfoFile)
    platforms = [p['name'] for p in read['platforms']]
    if 'All' in platforms:
        platforms = allPlatforms.values()

    projectname=os.path.split(metainfo)[-2]
    p = resolver.retrieveProject( projectname )
    if not p: # could not find the project for some reason
        continue

    ignoredPlatforms = [allPlatforms[ignoredPlatform] for ignoredPlatform in p.ignoredOnPlatforms]
    for ip in ignoredPlatforms:
        if ip in platforms:
            print('%s: %s is disabled in build.kde.org' % (projectname, ip))
